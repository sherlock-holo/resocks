package resocks.connection

import resocks.ResocksException

class ConnectionException(reason: String) : ResocksException(reason)